
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Events } from '/imports/api/events/collection.js';
import { Subscriptions } from '/imports/api/subscriptions/collection.js';

Meteor.methods({
  'emails.sendConfirmation'(subscriptionId) {

    check(subscriptionId, String);

    const subscription = Subscriptions.findOne({_id: subscriptionId});
    if (!subscription)
      throw new Meteor.Error('Could not find the subscription with id ' + subscriptionId);

    const event = Events.findOne({_id: subscription.event});
    if (!event)
      throw new Meteor.Error('Could not find the event with id ' + subscription.event);

    this.unblock();

    // TODO : use templates instead
    const to = subscription.legalGuardianData.email;
    const from = 'ramioul.application@gmail.com';
    const subject = 'Confirmation d\'inscription : ' + event.title;
    const text = "Madame, Monsieur,\n" +
      "Nous vous remercions pour l'inscription au stage " + event.title +
      " du " + Formatter.formatDate(event.startsAt, true) +
      " au " + Formatter.formatDate(event.endsAt, true) + ", pour votre enfant " +
      subscription.firstName + " " + subscription.lastName + ".\n" +
      "Merci de payer sur le compte BE42 352257 26 le montant de " +
      Formatter.formatPrice(event.price) + " avec la communication structurée suivante " +
      event.paymentCommunication + ".\n\n" +
      "Bien à vous,\n\n" +
      "L'équipe des animateurs";

    Email.send({ to, from, subject, text });
  },
  'emails.sendReminder'(subscriptionId) {

    if (!Roles.userIsInRole(Meteor.userId, ['admin'], 'ramioul'));

    check(subscriptionId, String);

    const subscription = Subscriptions.findOne({_id: subscriptionId});
    if (!subscription)
      throw new Meteor.Error('Could not find the subscription with id ' + subscriptionId);

    const event = Events.findOne({_id: subscription.event});
    if (!event)
      throw new Meteor.Error('Could not find the event with id ' + subscription.event);

    this.unblock();

    // TODO : use templates instead
    const to = subscription.legalGuardianData.email;
    const from = 'ramioul.application@gmail.com';
    const subject = 'Rappel de payement : ' + event.title;
    const text = "Madame, Monsieur,\n" +
      "Sauf erreur de notre part, nous n'avons pas le reçu le paiement de " +
      Formatter.formatPrice(event.price) + " pour  l'inscription au stage " +
      event.title +
      " du " + Formatter.formatDate(event.startsAt, true) +
      " au " + Formatter.formatDate(event.endsAt, true) + ", " +
      "pour votre enfant " +
      subscription.firstName + " " + subscription.lastName + ".\n" +
      "Merci de payer le plus rapidement sur le compte BE42 352257 26 le montant de " +
      Formatter.formatPrice(event.price) + " avec la communication structurée suivante " +
      event.paymentCommunication + ".\n" +
      "Sans quoi nous ne pourrons garantir la place de votre enfant.\n" +
      "Bien à vous,\n\n" +
      "L'équipe des animateurs";

    Email.send({ to, from, subject, text });
  },
});
