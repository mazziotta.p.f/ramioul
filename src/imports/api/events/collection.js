
import { Mongo } from 'meteor/mongo';

import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);

export const Events = new Mongo.Collection('events');
Events.attachSchema(new SimpleSchema({
  title: {
    type: String,
    label: "Titre",
    max: 200
  },
  description: {
    type: String,
    label: "Description",
    optional: true,
    max: 2000,
    autoform: {
      afFieldInput: {
        type: "textarea",
        rows: 10,
        class: "foo"
      }
    }
  },
  price: {
    type: Number,
    label: "Prix",
    min: 0
  },
  paymentCommunication: {
    type: String,
    label: "Communication structurée",
    max: 21
  },
  quota: {
    type: Number,
    label: "Quota",
    min: 0
  },
  startsAt: {
    type: Date,
    label: "Date de début",
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        timezoneId: "Europe/Paris",
      },
      dateTimePickerOptions: {
        format: 'DD/MM/YYYY',
        language: "fr-FR",
      }
    }
  },
  endsAt: {
    type: Date,
    label: "Date de fin",
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        timezoneId: "Europe/Paris",
      },
      dateTimePickerOptions: {
        format: 'DD/MM/YYYY',
        language: "fr-FR",
      }
    }
  },
  createdAt: {
    type: Date,
    optional: true
  },
}, { tracker: Tracker }));
