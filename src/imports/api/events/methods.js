
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Events } from '/imports/api/events/collection.js';

Meteor.methods({
  'events.remove'(eventId) {

    if (!Roles.userIsInRole(Meteor.userId(), ['admin'], 'ramioul')) return;

    check(eventId, String);

    Events.remove({_id: eventId});
  },
});
