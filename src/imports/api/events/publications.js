
import { Meteor } from 'meteor/meteor';

import { Events } from '/imports/api/events/collection.js';
import { Subscriptions } from '/imports/api/subscriptions/collection.js';

Meteor.publish('events', function () {
  return Events.find();
});

Meteor.publish('availableEvents', function () {
  const now = new Date();
  var events = Events.find({endsAt: { $gte: now }}, {sort: {startsAt: -1}});
  events.forEach(e => {
    let subscriptionsCount = Subscriptions.find({event: e._id}).count();
    if (subscriptionsCount >= e.quota) e.isFull = true;
    else e.isFull = false;
    this.added('events', e._id, e)
  });
  this.ready();
});
