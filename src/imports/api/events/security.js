
import { Events } from '/imports/api/events/collection.js';

Events.permit(['insert', 'update', 'remove']).ifHasRole({role: ['admin'], group: 'ramioul'}).allowInClientCode();
