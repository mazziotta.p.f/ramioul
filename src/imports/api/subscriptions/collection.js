
import { Mongo } from 'meteor/mongo';

import SimpleSchema from 'simpl-schema';
SimpleSchema.extendOptions(['autoform']);

import { Events } from '/imports/api/events/collection.js';

export const Subscriptions = new Mongo.Collection('subscriptions');
Subscriptions.attachSchema(new SimpleSchema({
  isPaid: {
    type: Boolean,
    label: "L'inscription est payée",
    optional: true,
    autoValue: function() {
      if ( !Roles.userIsInRole(this.userId, ['admin'], 'ramioul') ) {
        return false;
      }
      if (!this.value) return false;
    }
  },
  event: {
    type: String,
    label: "Événement",
    autoform: {
      options: () => Events.find().map( event => {
        return {
          label: event.title,
          value: event._id
        }
      }),
      firstOption: "(Faites votre choix)",
    },
  },
  firstName: {
    type: String,
    label: "Prénom",
    max: 200
  },
  lastName: {
    type: String,
    label: "Nom",
    max: 200
  },
  birthDate: {
    type: Date,
    label: "Date de naissance",
    autoform: {
      afFieldInput: {
        type: "bootstrap-datetimepicker",
        timezoneId: "Europe/Paris",
      },
      dateTimePickerOptions: {
        format: 'DD/MM/YYYY',
        language: "fr-FR",
      }
    }
  },
  medicalData: {
    type: Object,
    label: "Données médicales",
  },
  'medicalData.doctorName': {
    type: String,
    label: "Nom du médecin traitant",
    max: 200
  },
  'medicalData.doctorPhone': {
    type: String,
    label: "Téléphone du médecin",
    regEx: SimpleSchema.RegEx.Phone,
    min: 8,
    max: 14,
  },
  'medicalData.indications': {
    type: String,
    label: "Indications médicales particulières (médicaments, allergies, etc)",
    optional: true,
    max: 2000,
    autoform: {
      afFieldInput: {
        type: "textarea",
        rows: 10,
        class: "foo"
      }
    }
  },
  'medicalData.isVaccinatedAgainstTetanus': {
    type: Boolean,
    label: "Vacciné contre le tétanos",
  },
  legalGuardianData: {
    type: Object,
    label: "Personne responsable"
  },
  'legalGuardianData.firstName': {
    type: String,
    label: "Prénom de la personne responsable",
    max: 200
  },
  'legalGuardianData.LastName': {
    type: String,
    label: "Nom de la personne responsable",
    max: 200
  },
  'legalGuardianData.kinship': {
    type: String,
    label: "Lien de parenté",
    autoform: {
      options: [
        {label: "mère", value: "mère"},
        {label: "père", value: "père"},
        {label: "tuteur", value: "tuteur"},
      ],
      firstOption: "(Faites votre choix)",
    },
  },
  'legalGuardianData.address': {
    type: String,
    label: "Adresse postale",
    max: 200
  },
  'legalGuardianData.phone': {
    type: String,
    label: "Numéro de téléphone",
    regEx: SimpleSchema.RegEx.Phone,
    min: 8,
    max: 14,
  },
  'legalGuardianData.email': {
    type: String,
    regEx: SimpleSchema.RegEx.Email,
    label: "Adresse e-mail",
    max: 200
  },
  'legalGuardianData.sendBy': {
    type: String,
    label: "Je désire recevoir la facture et les informations pratiques pour le stage par",
    autoform: {
      options: [{label: 'e-mail', value: 'e-mail'}, {label: 'courrier', value: 'courrier'}],
      firstOption: "(Faites votre choix)",
    }
  },
  'legalGuardianData.iban': {
    type: String,
    label: "Numéro de compte IBAN",
    min: 10,
    max: 34,
    optional: true,
    custom: function () {
      if (!this.value) return;
      if (!IBAN.isValid(this.value)) return "Ce numéro IBAN est invalide";
    }
  },
  trustedIndividuals: {
    type: Array,
    label: "Personnes de confiance",
    optional: true
  },
  'trustedIndividuals.$': {
    type: Object,
    label: "Personne de confiance",
  },
  'trustedIndividuals.$.firstName': {
    type: String,
    label: "Prénom",
    max: 200
  },
  'trustedIndividuals.$.lastName': {
    type: String,
    label: "Nom",
    max: 200
  },
  agreesTerms: {
    type: Boolean,
    label: "Je, soussigné, mère, père, tuteur de l’enfant autorise les responsables du Préhistomuseum à prendre toutes les mesures d’urgence en cas d’accident survenu à mon enfant. ",
    custom: function () {
      if (this.value === false) return "required";
    }
  },
  createdAt: {
    type: Date,
    optional: true
  },
}, { tracker: Tracker }));
