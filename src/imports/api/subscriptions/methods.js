
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

import { Subscriptions } from '/imports/api/subscriptions/collection.js';

Meteor.methods({
  'subscriptions.remove'(subscriptionId) {

    if (!Roles.userIsInRole(Meteor.userId(), ['admin'], 'ramioul')) return;

    check(subscriptionId, String);

    Subscriptions.remove({_id: subscriptionId});
  },
  'subscriptions.togglePaid'(subscriptionId) {

    if (!Roles.userIsInRole(Meteor.userId(), ['admin'], 'ramioul')) return;

    check(subscriptionId, String);

    const subscription = Subscriptions.findOne({_id: subscriptionId});
    if (!subscription)
      throw new Meteor.Error('Could not find the subscription with id ' + subscriptionId);

    Subscriptions.update({_id: subscriptionId}, {$set: {isPaid: !subscription.isPaid}});
  },
});
