
import { Meteor } from 'meteor/meteor';

import { Subscriptions } from '/imports/api/subscriptions/collection.js';

Meteor.publish('subscriptions', function () {
  if (!Meteor.userId) return;
  return Subscriptions.find();
});
