
import { Subscriptions } from '/imports/api/subscriptions/collection.js';

Subscriptions.permit(['insert', 'update', 'remove']).ifHasRole({role: ['admin'], group: 'ramioul'}).allowInClientCode();
Subscriptions.permit(['insert']).allowInClientCode();
