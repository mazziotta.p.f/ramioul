
import { Template } from 'meteor/templating';
import Tabular from 'meteor/aldeed:tabular';
import { Events } from '/imports/api/events/collection.js';

new Tabular.Table({
  name: 'Events',
  collection: Events,
  columns: [
    {data: 'title', title: 'Titre'},
    {data: 'startsAt', title: 'Date de début', render: Formatter.formatDate},
    {data: 'endsAt', title: 'Date de fin', render: Formatter.formatDate},
    {data: '_id', title: 'Nombre d\'inscrits', tmpl: Meteor.isClient && Template.eventSubscriptionsCountCell},
    {data: 'quota', title: 'Quota'},
    {data: 'price', title: 'Prix par enfant', render: Formatter.formatPrice},
    {data: 'createdAt', title: 'Date de création', render: Formatter.formatDate},
    {title: 'Voir', tmpl: Meteor.isClient && Template.eventDisplayCell},
    {title: 'Modifier', tmpl: Meteor.isClient && Template.eventEditCell},
    {title: 'Supprimer', tmpl: Meteor.isClient && Template.eventRemoveCell}
  ],
  allow(userId) {
    return Roles.userIsInRole(userId, ['admin'], 'ramioul');
  },
});
