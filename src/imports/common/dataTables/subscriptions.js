
import { Template } from 'meteor/templating';
import Tabular from 'meteor/aldeed:tabular';
import { Events } from '/imports/api/events/collection.js';
import { Subscriptions } from '/imports/api/subscriptions/collection.js';

new Tabular.Table({
  name: 'Subscriptions',
  collection: Subscriptions,
  columns: [
    {data: 'event', title: 'Événement', tmpl: Meteor.isClient && Template.eventNameCell},
    {data: 'firstName', title: 'Prénom'},
    {data: 'lastName', title: 'Nom'},
    {data: 'isPaid', title: 'Payé', tmpl: Meteor.isClient && Template.subscriptionIsPaidCell},
    {data: 'createdAt', title: 'Date d\'inscription', render: Formatter.formatDate},
    {title: 'Voir', tmpl: Meteor.isClient && Template.subscriptionDisplayCell},
    {title: 'Modifier', tmpl: Meteor.isClient && Template.subscriptionEditCell},
    {title: 'Supprimer', tmpl: Meteor.isClient && Template.subscriptionRemoveCell}
  ],
  allow(userId) {
    return Roles.userIsInRole(userId, ['admin'], 'ramioul');
  },
});
