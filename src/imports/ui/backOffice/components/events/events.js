
import { Events } from '/imports/api/events/collection.js';

Template.eventsManager.events({
  'click [data-action="export"]'(event, template) {
    var csvContent = CSV.unparse(Events.find().fetch());
    window.open('data:text/csv;charset=utf-8,' + escape(csvContent), '_self');
  }
});

Template.eventRemoveCell.events({
  'click [data-action="remove"]'(event, template) {
    if (confirm('Supprimer cet évènement ?\n' + template.data.title))
      Meteor.call('events.remove', template.data._id);
  },
});

AutoForm.addHooks('insertEvents', {
  before: {
    insert: function(doc) {
      doc.createdAt = new Date();
      this.result(doc);
    }
  },
  after: {
    insert: (error, result) => { if (!error) Router.go('eventsManager') },
  },
});

AutoForm.addHooks('updateEvents', {
  after: {
    update: (error, result) => { if (!error) Router.go('eventsManager') },
  },
});
