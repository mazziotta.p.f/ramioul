
import { Subscriptions } from '/imports/api/subscriptions/collection.js';

Template.eventSubscriptionsCountCell.helpers({
  eventSubscriptionsCount: function() {
    return Subscriptions.find({event: this._id}).count();
  },
});
