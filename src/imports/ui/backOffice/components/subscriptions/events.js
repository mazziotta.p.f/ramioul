
import { Subscriptions } from '/imports/api/subscriptions/collection.js';

Template.subscriptionsManager.onCreated(function() {
  this.paidFilter = new ReactiveVar({});
  this.eventFilter = new ReactiveVar({});
});

Template.subscriptionsManager.events({
  'click [data-action="export"]'(event, template) {
    let filter = Object.assign({}, template.paidFilter.get(), template.eventFilter.get());
    let subscriptions = Subscriptions.find(filter).fetch();
    let data = [];
    subscriptions.forEach(subscription => {
      if (subscription.medicalData.indications)
        subscription.medicalData.indications = subscription.medicalData.indications.replace(/[\n\r]+/g, ' ');
      subscription.birthDate = Formatter.formatDate(subscription.birthDate, true);
      subscription.createdAt = Formatter.formatDate(subscription.createdAt, true);
      console.log(subscription);
      data.push(flattenObject(subscription));
    });
    let csvContent = CSV.unparse(data);
    window.open('data:text/csv;charset=utf-8,' + escape(csvContent), '_self');
  },
  'change [data-view-action="filter-event"]'(event, template) {
    if (!event.target.value)
      template.eventFilter.set({});
    else
      template.eventFilter.set({event: event.target.value});
  },
  'click [data-view-action="clear-filter"]'(event, template) {
    template.paidFilter.set({});
  },
  'click [data-view-action="filter-paid"]'(event, template) {
    template.paidFilter.set({isPaid: true});
  },
  'click [data-view-action="filter-notPaid"]'(event, template) {
    template.paidFilter.set({isPaid: false});
  },
});

Template.subscriptionIsPaidCell.events({
  'click [data-action="send-reminder"]'(event, template) {
    let message = 'Envoyer un rappel à cette inscription ?\n'
      + template.data.firstName + ' ' + template.data.lastName;
    if (confirm(message))
      Meteor.call('emails.sendReminder', template.data._id);
  },
  'click [data-action="toggle-paid"]'(event, template) {
    Meteor.call('subscriptions.togglePaid', template.data._id);
  }
});

Template.subscriptionRemoveCell.events({
  'click [data-action="remove"]'(event, template) {
    let message = 'Supprimer cette inscription ?\n'
      + template.data.firstName + ' ' + template.data.lastName;
    if (confirm(message))
      Meteor.call('subscriptions.remove', template.data._id);
  },
});

AutoForm.addHooks('insertSubscriptions', {
  before: {
    insert: function(doc) {
      doc.createdAt = new Date();
      this.result(doc);
    }
  },
  after: {
    insert: (error, result) => { if (!error) Router.go('subscriptionsManager') },
  },
});

AutoForm.addHooks('updateSubscriptions', {
  after: {
    update: (error, result) => { if (!error) Router.go('subscriptionsManager') },
  },
});

function flattenObject(data) {
    var result = {};
    function recurse (cur, prop) {
        if (Object(cur) !== cur) {
            result[prop] = cur;
        } else if (Array.isArray(cur)) {
             for(var i=0, l=cur.length; i<l; i++)
                 recurse(cur[i], prop + "[" + i + "]");
            if (l == 0)
                result[prop] = [];
        } else {
            var isEmpty = true;
            for (var p in cur) {
                isEmpty = false;
                recurse(cur[p], prop ? prop+"."+p : p);
            }
            if (isEmpty && prop)
                result[prop] = {};
        }
    }
    recurse(data, "");
    return result;
}
