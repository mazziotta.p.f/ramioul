
import { Events } from '/imports/api/events/collection.js';

Template.subscriptionsManager.helpers({
  selector: function() {
    let instance = Template.instance();
    return Object.assign({}, instance.paidFilter.get(), instance.eventFilter.get());
  },
});

Template.eventNameCell.helpers({
  eventName: function() {
    let event = Events.findOne({_id: this.event});
    if (event) return event.title;
  },
});
