
BackOfficeController = RouteController.extend({
  layoutTemplate: 'backOfficeLayout',
  authTemplate: 'loadingBackOffice',
  authRoute: 'home',
  protected: true,
  allowedRoles: ['admin'],
  allowedGroup: 'ramioul',
  onAfterAction: function() {
    clearTimeout(document.redirectHomeAtTimeout);
  },
  after: function() {
    window.scrollTo(0, 0);
  },
});
