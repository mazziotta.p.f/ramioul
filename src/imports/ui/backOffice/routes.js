
import { Events } from '/imports/api/events/collection.js';
import { Subscriptions } from '/imports/api/subscriptions/collection.js';

Router.route('/manage/', {
  controller: 'BackOfficeController',
  name: 'backOfficeHome',
  subscriptions: () => {
    Meteor.subscribe('events');
    Meteor.subscribe('subscriptions');
  },
  onAfterAction: function() {
    document.title = 'Ramioul ~ manager';
  },
});

Router.route('/manage/events', {
  controller: 'BackOfficeController',
  name: 'eventsManager',
  subscriptions: () => {
    Meteor.subscribe('events');
    Meteor.subscribe('subscriptions');
  },
  data: {
    eventsCollection: () => Events,
  },
  onAfterAction: function() {
    document.title = 'Ramioul ~ manager > events';
  },
});

Router.route('/manage/events/detail/:eventId', {
  controller: 'BackOfficeController',
  name: 'eventsManagerDetail',
  subscriptions: () => {
    Meteor.subscribe('events');
    Meteor.subscribe('subscriptions');
  },
  data: {
    event: () => Events.findOne({_id: Router.current().params.eventId}),
  },
  onAfterAction: function() {
    document.title = 'Ramioul ~ manager > events > detail';
  },
});

Router.route('/manage/events/add', {
  controller: 'BackOfficeController',
  name: 'eventsManagerAdd',
  subscriptions: () => {
    Meteor.subscribe('events');
    Meteor.subscribe('subscriptions');
  },
  data: {
    eventsCollection: () => Events,
  },
  onAfterAction: function() {
    document.title = 'Ramioul ~ manager > events > add';
  },
});

Router.route('/manage/events/update/:eventId', {
  controller: 'BackOfficeController',
  name: 'eventsManagerUpdate',
  subscriptions: () => {
    Meteor.subscribe('events');
    Meteor.subscribe('subscriptions');
  },
  data: {
    eventsCollection: () => Events,
    event: () => Events.findOne({_id: Router.current().params.eventId}),
  },
  onAfterAction: function() {
    document.title = 'Ramioul ~ manager > events > update';
  },
});

Router.route('/manage/subscriptions', {
  controller: 'BackOfficeController',
  name: 'subscriptionsManager',
  subscriptions: () => {
    Meteor.subscribe('events');
    Meteor.subscribe('subscriptions');
  },
  data: {
    subscriptionsCollection: () => Subscriptions,
    allEvents: () => Events.find(),
  },
  onAfterAction: function() {
    document.title = 'Ramioul ~ manager > subscriptions';
  },
});

Router.route('/manage/subscriptions/detail/:subscriptionId', {
  controller: 'BackOfficeController',
  name: 'subscriptionsManagerDetail',
  data: {
    subscription: () => Subscriptions.findOne({_id: Router.current().params.subscriptionId}),
  },
  onAfterAction: function() {
    document.title = 'Ramioul ~ manager > subscriptions > detail';
  },
});

Router.route('/manage/subscriptions/add', {
  controller: 'BackOfficeController',
  name: 'subscriptionsManagerAdd',
  subscriptions: () => {
    Meteor.subscribe('events');
    Meteor.subscribe('subscriptions');
  },
  data: {
    subscriptionsCollection: () => Subscriptions,
  },
  onAfterAction: function() {
    document.title = 'Ramioul ~ manager > subscriptions';
  },
});

Router.route('/manage/subscriptions/update/:subscriptionId', {
  controller: 'BackOfficeController',
  name: 'subscriptionsManagerUpdate',
  subscriptions: () => {
    Meteor.subscribe('events');
    Meteor.subscribe('subscriptions');
  },
  data: {
    subscriptionsCollection: () => Subscriptions,
    subscription: () => Subscriptions.findOne({_id: Router.current().params.subscriptionId}),
  },
  onAfterAction: function() {
    document.title = 'Ramioul ~ manager > subscriptions > update';
  },
});
