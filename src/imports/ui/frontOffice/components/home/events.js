
Template.event.events({
  'click [data-action="subscribe"]'(event, template) {
    Router.go('subscribe', {eventId: template.data._id});
  },
});
