
AutoForm.addHooks('subscribeChild', {
  before: {
    insert: function(doc) {
      doc.createdAt = new Date();
      doc.event = Router.current().params.eventId;
      doc.isPaid = false;
      this.result(doc);
    }
  },
  after: {
    insert: (error, result) => {
      if (!error) {
        Meteor.call('emails.sendConfirmation', result);
        Router.go('confirmation');
      }
    },
  },
});
