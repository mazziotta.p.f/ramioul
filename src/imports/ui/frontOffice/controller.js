
FrontOfficeController = RouteController.extend({
  layoutTemplate: 'frontOfficeLayout',
  after: function() {
    window.scrollTo(0, 0);
  },
});
