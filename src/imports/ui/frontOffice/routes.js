
import { Events } from '/imports/api/events/collection.js';
import { Subscriptions } from '/imports/api/subscriptions/collection.js';

Router.route('/', {
  controller: 'FrontOfficeController',
  name: 'home',
  template: 'home',
  subscriptions: () => {
    Meteor.subscribe('availableEvents');
  },
  data: {
    allEvents: () => Events.find(),
  },
  onAfterAction: function() {
    document.title = 'Ramioul ~ accueil';
  },
});

Router.route('/inscription/:eventId', {
  controller: 'FrontOfficeController',
  name: 'subscribe',
  waitOn: () => {
    Meteor.subscribe('availableEvents');
  },
  data: {
    subscriptionsCollection: () => Subscriptions,
    currentEvent: () => Events.findOne({_id: Router.current().params.eventId}),
  },
  onBeforeAction: function() {
    const event = Events.findOne({_id: Router.current().params.eventId});
    if (event && event.isFull) Router.go('eventIsFull');
    this.next();
  },
  onAfterAction: function() {
    document.title = 'Ramioul ~ inscription';
  },
});

Router.route('/confirmation', {
  controller: 'FrontOfficeController',
  name: 'confirmation',
  onAfterAction: function() {
    document.title = 'Ramioul ~ inscription > confirmation';
  },
});

Router.route('/evenement-complet', {
  controller: 'FrontOfficeController',
  name: 'eventIsFull',
  onAfterAction: function() {
    document.title = 'Ramioul ~ inscription > événement complet';
  },
});
