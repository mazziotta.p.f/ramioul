
import moment from 'moment';

class ValuesFormatter {
  formatDate(value, short) {
    if (value instanceof Date)
      if (short)
        return moment(value).format('DD/MM/YYYY');
      else
        return moment(value).format('DD/MM/YYYY HH:mm');
    return value;
  }

  formatPrice(value) {
    if (value instanceof Number)
      return value.toFixed(2) + ' €';
    let price = parseFloat(value);
    return price.toFixed(2) + ' €';
  }

  formatParagraphs(value) {
    return '<p>' + value.split('\n').join('</p><p>') + '</p>';
  }
}

Formatter = new ValuesFormatter();

if (Meteor.isClient) {
  Template.registerHelper( 'formatDate', (value, short) => Formatter.formatDate(value, short));
  Template.registerHelper( 'formatPrice', (value) => Formatter.formatPrice(value));
  Template.registerHelper( 'formatParagraphs', (value) => Formatter.formatParagraphs(value));
}
