
// API Security
import '/imports/api/events/security.js';
import '/imports/api/subscriptions/security.js';

// API Collections
import '/imports/api/events/collection.js';
import '/imports/api/subscriptions/collection.js';

// API methods
import '/imports/api/events/methods.js';
import '/imports/api/subscriptions/methods.js';
import '/imports/api/emails/methods.js';

// API publications
import '/imports/api/events/publications.js';
import '/imports/api/subscriptions/publications.js';

// Fixtures
import '/imports/api/users/fixtures.js';

// Server & client side routes
import '/imports/ui/backOffice/routes.js';
import '/imports/ui/frontOffice/routes.js';

// DataTables
import '/imports/common/dataTables/events.js';
import '/imports/common/dataTables/subscriptions.js';

// Server side config
import '/imports/startup/accounts.js';
import '/imports/startup/emails.js';
